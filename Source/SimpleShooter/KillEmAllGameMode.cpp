// Fill out your copyright notice in the Description page of Project Settings.


#include "KillEmAllGameMode.h"

#include "EngineUtils.h"
#include "ShooterAIController.h"
#include "Kismet/GameplayStatics.h"

void AKillEmAllGameMode::BeginPlay()
{
	Super::BeginPlay();
	if(BackgroundMusic)
	{
		UGameplayStatics::PlaySound2D(this, BackgroundMusic, MusicVolume);
	}
}

void AKillEmAllGameMode::PawnKilled(APawn* PawnKilled)
{
	Super::PawnKilled(PawnKilled);
	if (Cast<APlayerController>(PawnKilled->GetController()))
	{
		EndGame(false);
	}
	UE_LOG(LogTemp, Display, TEXT("%i"), GetAIEnemies())
	if(GetAIEnemies() == 0)
	{
		EndGame(true);
	}
}

void AKillEmAllGameMode::EndGame(const bool bIsPlayerWinner) const
{
	for (AController* Controller : TActorRange<AController>(GetWorld()))
	{
		const bool bIsWinner = Controller->IsPlayerController() == bIsPlayerWinner;		
		Controller->GameHasEnded(Controller->GetPawn(), bIsWinner);
	}
}

int32 AKillEmAllGameMode::GetAIEnemies() const
{
	TArray<AShooterAIController*> ShooterAIs;
	for(AShooterAIController* ShooterAI :  TActorRange<AShooterAIController>(GetWorld()))
	{
		ShooterAIs.Add(ShooterAI);
	}
	return ShooterAIs.Num();
}
