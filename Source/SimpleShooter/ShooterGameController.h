// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "ShooterGameController.generated.h"


UCLASS()
class SIMPLESHOOTER_API AShooterGameController : public APlayerController
{
	GENERATED_BODY()
	
	protected:
	virtual void BeginPlay() override;
	
public:
	virtual void GameHasEnded(AActor* EndGameFocus, bool bIsWinner) override;

private:
	UPROPERTY(EditAnywhere)
	TSubclassOf<UUserWidget> LoseScreenWidget;
	
	UPROPERTY(EditAnywhere)
	TSubclassOf<UUserWidget> WinScreenWidget;
	
	UPROPERTY(EditAnywhere)
	TSubclassOf<UUserWidget> HUDWidget;
	
	UPROPERTY()
	UUserWidget* HUD;
	
	UPROPERTY(EditAnywhere)
	float RestartDelay = 5.0f;

	FTimerHandle RestartTimer;
};
