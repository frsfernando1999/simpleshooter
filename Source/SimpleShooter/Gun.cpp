// Fill out your copyright notice in the Description page of Project Settings.


#include "Gun.h"

#include "Kismet/GameplayStatics.h"

// Sets default values
AGun::AGun()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));
	RootComponent = Root;

	MeshComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Gun Mesh"));
	MeshComponent->SetupAttachment(Root);
}

void AGun::PullTrigger()
{
	FireAnimationAndSound();

	FHitResult HitResult;
	FVector ShotDirection;
	if (GunTrace(HitResult, ShotDirection))
	{
		FireImpactSoundAndAnimation(HitResult, ShotDirection);
		//DrawDebugSphere(GetWorld(), HitResult.ImpactPoint, 20,  12, FColor::Red, false, 5);
		if (AActor* HitActor = HitResult.GetActor())
		{
			FPointDamageEvent DamageEvent(Damage, HitResult, ShotDirection, nullptr);
			HitActor->TakeDamage(Damage, DamageEvent, Cast<APawn>(GetOwner())->GetController(), this);
		}
	}
}

bool AGun::GunTrace(FHitResult& HitResult, FVector& ShotDirection) const
{
	const APawn* OwnerPawn = Cast<APawn>(GetOwner());
	if (OwnerPawn == nullptr)
	{
		return false;
	}
	if (const AController* OwnerController = OwnerPawn->GetController())
	{
		FVector ViewportLocation;
		FRotator ViewportRotation;

		OwnerController->GetPlayerViewPoint(ViewportLocation, ViewportRotation);

		const FVector End = ViewportLocation + ViewportRotation.Vector() * MaxRange;
		ShotDirection = -ViewportRotation.Vector();

		FCollisionQueryParams Params;
		Params.AddIgnoredActor(this);
		Params.AddIgnoredActor(GetOwner());
		return GetWorld()->LineTraceSingleByChannel(HitResult, ViewportLocation, End, ECC_GameTraceChannel1, Params);
	}
	return false;
}

void AGun::FireAnimationAndSound() const
{
	if (FireParticles)
	{
		UGameplayStatics::SpawnEmitterAttached(FireParticles, MeshComponent, TEXT("MuzzleFlashSocket"));
	}
	if (MuzzleSound)
	{
		UGameplayStatics::SpawnSoundAttached(MuzzleSound, MeshComponent, TEXT("MuzzleFlashSocket"));
	}
}

void AGun::FireImpactSoundAndAnimation(const FHitResult& HitResult, const FVector& ShotDirection) const
{
	if (HitParticles)
	{
		UGameplayStatics::SpawnEmitterAtLocation(this, HitParticles, HitResult.ImpactPoint,
		                                         ShotDirection.Rotation());
	}
	if (HitSound)
	{
		UGameplayStatics::SpawnSoundAtLocation(this, HitSound, HitResult.ImpactPoint, ShotDirection.Rotation());
	}
}
