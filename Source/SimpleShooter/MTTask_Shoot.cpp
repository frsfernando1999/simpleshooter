// Fill out your copyright notice in the Description page of Project Settings.


#include "MTTask_Shoot.h"

#include "AIController.h"
#include "ShooterCharacter.h"

UMTTask_Shoot::UMTTask_Shoot()
{
	NodeName = TEXT("Shoot");
}

EBTNodeResult::Type UMTTask_Shoot::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	Super::ExecuteTask(OwnerComp, NodeMemory);
	if (AShooterCharacter* AIPawn = Cast<AShooterCharacter>(OwnerComp.GetAIOwner()->GetPawn()))
	{
		AIPawn->Fire();
		return EBTNodeResult::Succeeded;
	}
	return EBTNodeResult::Failed;
}
