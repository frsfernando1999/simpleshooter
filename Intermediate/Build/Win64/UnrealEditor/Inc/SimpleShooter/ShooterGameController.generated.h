// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SIMPLESHOOTER_ShooterGameController_generated_h
#error "ShooterGameController.generated.h already included, missing '#pragma once' in ShooterGameController.h"
#endif
#define SIMPLESHOOTER_ShooterGameController_generated_h

#define FID_SimpleShooter_Source_SimpleShooter_ShooterGameController_h_13_SPARSE_DATA
#define FID_SimpleShooter_Source_SimpleShooter_ShooterGameController_h_13_RPC_WRAPPERS
#define FID_SimpleShooter_Source_SimpleShooter_ShooterGameController_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_SimpleShooter_Source_SimpleShooter_ShooterGameController_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAShooterGameController(); \
	friend struct Z_Construct_UClass_AShooterGameController_Statics; \
public: \
	DECLARE_CLASS(AShooterGameController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SimpleShooter"), NO_API) \
	DECLARE_SERIALIZER(AShooterGameController)


#define FID_SimpleShooter_Source_SimpleShooter_ShooterGameController_h_13_INCLASS \
private: \
	static void StaticRegisterNativesAShooterGameController(); \
	friend struct Z_Construct_UClass_AShooterGameController_Statics; \
public: \
	DECLARE_CLASS(AShooterGameController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SimpleShooter"), NO_API) \
	DECLARE_SERIALIZER(AShooterGameController)


#define FID_SimpleShooter_Source_SimpleShooter_ShooterGameController_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AShooterGameController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AShooterGameController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AShooterGameController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AShooterGameController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AShooterGameController(AShooterGameController&&); \
	NO_API AShooterGameController(const AShooterGameController&); \
public:


#define FID_SimpleShooter_Source_SimpleShooter_ShooterGameController_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AShooterGameController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AShooterGameController(AShooterGameController&&); \
	NO_API AShooterGameController(const AShooterGameController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AShooterGameController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AShooterGameController); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AShooterGameController)


#define FID_SimpleShooter_Source_SimpleShooter_ShooterGameController_h_10_PROLOG
#define FID_SimpleShooter_Source_SimpleShooter_ShooterGameController_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_SimpleShooter_Source_SimpleShooter_ShooterGameController_h_13_SPARSE_DATA \
	FID_SimpleShooter_Source_SimpleShooter_ShooterGameController_h_13_RPC_WRAPPERS \
	FID_SimpleShooter_Source_SimpleShooter_ShooterGameController_h_13_INCLASS \
	FID_SimpleShooter_Source_SimpleShooter_ShooterGameController_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_SimpleShooter_Source_SimpleShooter_ShooterGameController_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_SimpleShooter_Source_SimpleShooter_ShooterGameController_h_13_SPARSE_DATA \
	FID_SimpleShooter_Source_SimpleShooter_ShooterGameController_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_SimpleShooter_Source_SimpleShooter_ShooterGameController_h_13_INCLASS_NO_PURE_DECLS \
	FID_SimpleShooter_Source_SimpleShooter_ShooterGameController_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SIMPLESHOOTER_API UClass* StaticClass<class AShooterGameController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_SimpleShooter_Source_SimpleShooter_ShooterGameController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
