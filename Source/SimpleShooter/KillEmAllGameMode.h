// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SimpleShooterGameModeBase.h"
#include "KillEmAllGameMode.generated.h"

/**
 * 
 */
UCLASS()
class SIMPLESHOOTER_API AKillEmAllGameMode final : public ASimpleShooterGameModeBase
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

public:
	virtual void PawnKilled(APawn* PawnKilled) override;

private:
	void EndGame(bool bIsPlayerWinner) const;
	int32 GetAIEnemies() const;
	
	UPROPERTY(EditAnywhere)
	float MusicVolume = 1;
	
	UPROPERTY(EditAnywhere)
	USoundBase* BackgroundMusic;
};
