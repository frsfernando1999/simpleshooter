// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SIMPLESHOOTER_MTTask_Shoot_generated_h
#error "MTTask_Shoot.generated.h already included, missing '#pragma once' in MTTask_Shoot.h"
#endif
#define SIMPLESHOOTER_MTTask_Shoot_generated_h

#define FID_SimpleShooter_Source_SimpleShooter_MTTask_Shoot_h_15_SPARSE_DATA
#define FID_SimpleShooter_Source_SimpleShooter_MTTask_Shoot_h_15_RPC_WRAPPERS
#define FID_SimpleShooter_Source_SimpleShooter_MTTask_Shoot_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_SimpleShooter_Source_SimpleShooter_MTTask_Shoot_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMTTask_Shoot(); \
	friend struct Z_Construct_UClass_UMTTask_Shoot_Statics; \
public: \
	DECLARE_CLASS(UMTTask_Shoot, UBTTaskNode, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SimpleShooter"), NO_API) \
	DECLARE_SERIALIZER(UMTTask_Shoot)


#define FID_SimpleShooter_Source_SimpleShooter_MTTask_Shoot_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUMTTask_Shoot(); \
	friend struct Z_Construct_UClass_UMTTask_Shoot_Statics; \
public: \
	DECLARE_CLASS(UMTTask_Shoot, UBTTaskNode, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SimpleShooter"), NO_API) \
	DECLARE_SERIALIZER(UMTTask_Shoot)


#define FID_SimpleShooter_Source_SimpleShooter_MTTask_Shoot_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMTTask_Shoot(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMTTask_Shoot) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMTTask_Shoot); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMTTask_Shoot); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMTTask_Shoot(UMTTask_Shoot&&); \
	NO_API UMTTask_Shoot(const UMTTask_Shoot&); \
public:


#define FID_SimpleShooter_Source_SimpleShooter_MTTask_Shoot_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMTTask_Shoot(UMTTask_Shoot&&); \
	NO_API UMTTask_Shoot(const UMTTask_Shoot&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMTTask_Shoot); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMTTask_Shoot); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMTTask_Shoot)


#define FID_SimpleShooter_Source_SimpleShooter_MTTask_Shoot_h_12_PROLOG
#define FID_SimpleShooter_Source_SimpleShooter_MTTask_Shoot_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_SimpleShooter_Source_SimpleShooter_MTTask_Shoot_h_15_SPARSE_DATA \
	FID_SimpleShooter_Source_SimpleShooter_MTTask_Shoot_h_15_RPC_WRAPPERS \
	FID_SimpleShooter_Source_SimpleShooter_MTTask_Shoot_h_15_INCLASS \
	FID_SimpleShooter_Source_SimpleShooter_MTTask_Shoot_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_SimpleShooter_Source_SimpleShooter_MTTask_Shoot_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_SimpleShooter_Source_SimpleShooter_MTTask_Shoot_h_15_SPARSE_DATA \
	FID_SimpleShooter_Source_SimpleShooter_MTTask_Shoot_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_SimpleShooter_Source_SimpleShooter_MTTask_Shoot_h_15_INCLASS_NO_PURE_DECLS \
	FID_SimpleShooter_Source_SimpleShooter_MTTask_Shoot_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SIMPLESHOOTER_API UClass* StaticClass<class UMTTask_Shoot>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_SimpleShooter_Source_SimpleShooter_MTTask_Shoot_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
