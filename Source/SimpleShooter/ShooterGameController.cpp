// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterGameController.h"

#include "Blueprint/UserWidget.h"

void AShooterGameController::BeginPlay()
{
	Super::BeginPlay();
	if (HUDWidget)
	{
		HUD = CreateWidget(this, HUDWidget);
		HUD->AddToViewport();
	}
}

void AShooterGameController::GameHasEnded(AActor* EndGameFocus, bool bIsWinner)
{
	Super::GameHasEnded(EndGameFocus, bIsWinner);

	if (bIsWinner)
	{
		if (WinScreenWidget)
		{
			UUserWidget* WinScreen = CreateWidget(this, WinScreenWidget);
			WinScreen->AddToViewport();
		}
	}
	else
	{
		if (LoseScreenWidget)
		{
			UUserWidget* LoseScreen = CreateWidget(this, LoseScreenWidget);
			LoseScreen->AddToViewport();
		}
	}
	if (HUD)
	{
		HUD->RemoveFromViewport();
	}
	GetWorldTimerManager().SetTimer(RestartTimer, this, &APlayerController::RestartLevel, RestartDelay);
}
